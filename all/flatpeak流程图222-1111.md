```mermaid
sequenceDiagram
    participant 用户
    participant 国际美居APP
    participant 云端
    participant 本地库
    participant Flatpeak
    participant IT中心

    用户->>国际美居APP: 选择国家
    activate 国际美居APP
    国际美居APP->>云端: 国家码
    activate 云端
    云端->>本地库: 获取供应商列表
    alt 本地库有数据
        本地库->>云端: 供应商列表
    else 本地库无数据
        云端->>Flatpeak: 获取供应商列表
        Flatpeak->>云端: 供应商列表
    end
    云端->>国际美居APP: 供应商列表
    deactivate 云端
    deactivate 国际美居APP

    用户->>国际美居APP: 选择用电计划
    activate 国际美居APP
    alt 用电计划存在
        国际美居APP->>云端: 用电计划
    else 用电计划不存在
        国际美居APP->>云端: 新增用电计划
        云端->>国际美居APP: 用电计划
    end
    deactivate 国际美居APP

    用户->>国际美居APP: 设置附加费
    activate 国际美居APP
    国际美居APP->>云端: 附加费
    deactivate 国际美居APP

    云端->>国际美居APP: 昨日、今日、明日每小时电价
    activate 国际美居APP
    用户->>国际美居APP: 点击确定
    deactivate 国际美居APP

    国际美居APP->>云端: 保存用电计划
    activate 云端
    云端->>Flatpeak: 获取 location_id
    Flatpeak->>云端: location_id
    deactivate 云端

    loop 每小时
        activate 云端
        云端->>Flatpeak: 查询电价 (location_id)
        Flatpeak->>云端: 电价
        alt 电价变动
            云端->>IT中心: 推送电价
        end
        deactivate 云端
    end
```